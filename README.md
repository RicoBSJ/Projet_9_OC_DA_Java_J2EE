# MyERP

## Organisation du répertoire

*   `doc` : documentation
*   `docker` : répertoire relatifs aux conteneurs _docker_ utiles pour le projet
*   `dev` : environnement de développement
*   `src` : code source de l'application

## Structure de l'application

*   La partie Interface Homme Machine n'est pas intégrée dans le code source de l'application.
*   Le code source fourni est exclusivement destiné à réaliser des tests.
*   L'application est constituée de 4 couches : la couche model, la couche business,
*   la couche technical et la couche consumer.

## Les tests

*   Les tests portent sur deux couches : la couche business et la couche model
*   Les tests portant sur la couche métier passent donc en revue les fonctionnalités
*   notamment ils vérifient que les règles de gestion comptables sont respectées.
*   Les tests portant les beans permettent de vérifier la bonne instanciation des objets
*   notamment celle de la regexp (référence d'une écriture comptable) contenue
*   dans la classe EcritureComptable.

## Intégration continue

*   Un système d'intégration continue est mis en place avec GitLab CI.

### Configuration

*   Le fichier ".gitlab-ci.yml" permet de lancer automatiquement les build et les tests

        image: maven:3.8.2-jdk-11
        stages:
           - build
           - test
        build:
           stage: build
           script:
               - cd src
               - mvn compile
        test:
           stage: test
           script:
               - cd src
               - mvn test

### Repository

*   Le code source de l'application peut être cloné à l'adresse suivante :

    https://gitlab.com/RicoBSJ/Projet_9_OC_DA_Java_J2EE.git
